# peripherals_lab_SW

The sdk workspace ready to write the code to manage the peripherals using interrupts.

# USAGE

Copy this repository in your local machine.

I'll find two folder corresponding to two different xSDK workspaces
- `lab_periph_zybo.sdk` targets the OLD ZYBO board
- `lab_periph_zyboZ7.sdk` targets the ZYBO Z7 board

Open xSDK and select one of these workspaces depending on the board you have.
