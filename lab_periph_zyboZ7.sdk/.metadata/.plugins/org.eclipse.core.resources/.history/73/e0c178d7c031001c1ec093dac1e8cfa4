/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#define SWI_BASEADDR  0x40000000
#define LED_BASEADDR  0x40010000
#define BUT_BASEADDR  0x40020000

#define INTC_BASEADDR 0x41200000


#define SWI_DATA     (SWI_BASEADDR + 0x0)

#define LED_DATA     (LED_BASEADDR + 0x0)
#define LED_TRISTATE (LED_BASEADDR + 0x4)

//Global interrupt enable register. L'aggettivo global si riferisce ai due canali della GPIO non all'intero sistema.
#define SWI_GIER (SWI_BASEADDR + 0x11C)
#define BUT_GIER (BUT_BASEADDR + 0x11C)

#define SWI_IPIER (SWI_BASEADDR + 0x128)
#define BUT_IPIER (BUT_BASEADDR + 0x128)

#define SWI_IPISR (SWI_BASEADDR + 0x120)
#define BUT_IPISR (BUT_BASEADDR + 0x120)

#define INTC_ISR (INTC_BASEADDR + 0x00)
#define INTC_IER (INTC_BASEADDR + 0x08)
#define INTC_IAR (INTC_BASEADDR + 0x0C)
#define INTC_MER (INTC_BASEADDR + 0x1C)

// definisco la posizione del bit associato alle due periferiche nel intc. La posizione è stata scelta da voi su vivado
#define SWI_INT 0
#define BUT_INT 1

void gestisci_interrupt (void) __attribute__ ((interrupt_handler)); //la funzione di gestione degli interrupt può avere qualsiasi nome, l'importante è l'attribute
void swi_func();
void but_func();

int main()
{

    init_platform();

    microblaze_enable_interrupts();


    // impostiamo la gpio dei led in scrittura
    *((volatile int *)LED_TRISTATE)= 0x0;


    //Attivazione degli interrupts nel INTC
    *((volatile int *)(INTC_IER))=0b11; // due bit entrambi a 1
    *((volatile int *)(INTC_MER))=0b11; // due bit entrambi a 1

    //Attivazione degli interrupts nel GPIO degli switch
    *((volatile int *)(SWI_IPIER))=1;
    *((volatile int *)(SWI_GIER)) = 0x1 << 31; // è equivalente a scrivere 0x80000000. Dal manuale, il GIER, viene impostato scrivendo nel bit più a sinistra


    //Attivazione degli interrupts nel GPIO del button (solo il button 1, lo 0 è il reset e gli altri sono scollegati
    *((volatile int *)(BUT_IPIER))=1;
    *((volatile int *)(BUT_GIER))=0x1 << 31;


    print("Test working \n\r"); // è bene usare spesso le print quando possibile per capire in quale parte dell'esecuzione si trova il processore

    while(1){;} // il core resta in eterno in questo while, a meno che non arrivi un interrupt....

    //ovviamente al posto del while(1) ci potrebbe essere una qualsiasi altra elaborazione, magari più utile.



    cleanup_platform();
    return 0;
}




void gestisci_interrupt (void){

    int isr_value;

    isr_value = *((volatile int *)(INTC_ISR));



    // bit shift + bit masking per ricavare il bit di stato associato alle periferiche.
    // Il contenuto del registro si può elaborare come si preferisce ma il bit masking e lo shift sono comunemente utilizzati
    if (((isr_value>>SWI_INT)&0x1) == 1){

    	// spengo l'interrupt dello switch. é sufficiente scrivere 1 nel bit più a destra  del registro ISR (nota: è un registro TOW)
    	*((volatile int *)(SWI_IPISR)) = 1;

    	//spengo l'interrupt nell'INTC. NOTA: prima spengo quello della periferica poi quello del controller. Non vale il viceversa altrimenti l'interrupt della periferica fa rialzare quello del controller.
    	*((volatile int *)(INTC_IAR)) = 1<<SWI_INT; // equivale a scrivere 0x1

    	// chiamo la funzione associata agli switch
    	swi_func();
    }

    if (((isr_value>>BUT_INT)&0x1) == 1){

    	// spengo l'interrupt del button. é sufficiente scrivere 1 nel bit più a destra  del registro ISR (nota: è un registro TOW)
    	*((volatile int *)(BUT_IPISR)) = 1;

    	//spengo l'interrupt nell'INTC.
    	*((volatile int *)(INTC_IAR)) = 1<<BUT_INT; //equivale a scrivere 0x2

    	// chiamo la funzione associata al button.
    	but_func();
    }
}

void swi_func(){
	print("Switch interrupt!\n\r");
    // come operazione ho scelto di accendere i led corrispondenti.
	*((volatile int *)LED_DATA)= *((volatile int *)SWI_DATA);
}
void but_func(){
	print("Button interrupt!\n\r");

	// il button spegne tutti i led
	*((volatile int *)LED_DATA)= 0;
}
